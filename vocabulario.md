# Vocabulário

## Alfabeto

A: [a:], como em *A*mor
B: [be:], como em *B*ola
C: [tse:], como em *TS*é-*TS*é
D: [de:], como em *D*ata
E: [e:], como em *E*quino
F: [εf], como em *F*eliz
G: [ge:], como em *G*arfo
H: [ha:], como em ca*RR*o
I: [i:], como em *I*r
J: [jot], como em *I*odo
K: [ka:], como em *C*asa
L: [εl], como em *L*eite
M: [εm], como em *M*odelo
N: [εn], como em *N*ada
O: [o:], como em *O*nda
P: [pe:], como em *P*alavra
Q: [ku:], como em *QU*anto
R: [εr], como em ca*R*o
S: [εs], como em ca*S*a
T: [te:], como em *T*empo
U: [u:], como em *U*va
V: [fau:], como em *F*arinha
W: [ve:], como em *V*ento
Y: [ypsilεn], como em *I*ate
Z: [tsεt], como em *TSÉ-*TSÉ*
Ä/ä: [ε:], como em h*É*lice
Ö/ö: [ø:], lábios de quem fala [o:], mas som de [e:]
Ü/ü: [y:], lábios de quem fala [u:], mas som de [i:] 
Ai/Ei: [ai], som de ái
Äu/Eu: [ɔi], som de ói, tipo Freud e Europa

## Pronomes pessoais

Ich: eu
Du: tu
Er: ele
Sie: ela
Es: terceira pessoa (neutro)
Wir: nós
Ihr: vós
sie: eles/elas
Sie: O Sr/a Sra

## Verbos

Sein: ser/estar
    Ich bin: eu sou
    Du bist: tu és
    Er/Sie/Es ist: ele/ela é
    Wir sind: nós somos
    Ihr seid: vós sois
    Sie sind: tu és (formal)

haben: ter
    Ich habe: eu tenho
    Du hast: tu tens
    Er/Sie/Es hat: ele/ela tem
    Wir haben: nós temos
    Ihr habt: vós tens
    Sie haben: tu tens (formal)


## Frases
